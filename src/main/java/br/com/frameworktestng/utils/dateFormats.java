package br.com.frameworktestng.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class dateFormats {

	public static Date inicioExec(String inicio) throws ParseException {
		String dataInicio = inicio;
		Date inicioExec = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(dataInicio);
		return inicioExec;
	}
	
	public static Date horario(String horario) throws ParseException {
		String horarioExec = horario;
		Date inicioExec = new SimpleDateFormat("HH:mm:ss").parse(horarioExec);
		return inicioExec;
	}
	
	public static int diaDate(Date date) throws ParseException {
		int dia;
		SimpleDateFormat sdf = new SimpleDateFormat("dd");
		dia = Integer.parseInt(sdf.format(date));
		return dia;
		
	}
	
	public static int diaSemana() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(atualDataHora());
        int diaDaSemana = gc.get(GregorianCalendar.DAY_OF_WEEK);
		return diaDaSemana;
	}
	
	public static Date atualDataHora() {
		Date dataAtual = new Date(System.currentTimeMillis());
		return dataAtual;
	}
	
	public static String atualDataHoraString() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date dataAtual = new Date(System.currentTimeMillis());
		return sdf.format(dataAtual);
	}
	
	public static Date atualHora() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		Date dataAtual = new Date(System.currentTimeMillis());
		Date inicioExec = new SimpleDateFormat("HH:mm:ss").parse( sdf.format(dataAtual));
		return inicioExec;
	}
	
	public static String datePrintScreen() {
		Date data = new Date();
		SimpleDateFormat formatador = new SimpleDateFormat("ddMMyyyyHHmmss");
		String dataAtual = formatador.format(data);
		return dataAtual;
	}
}
