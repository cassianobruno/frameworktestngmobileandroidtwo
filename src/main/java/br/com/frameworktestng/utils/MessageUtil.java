package br.com.frameworktestng.utils;

public class MessageUtil {

	private static String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	// prints the message
	public void printMessage() {
		System.out.println(message);
	}
}
