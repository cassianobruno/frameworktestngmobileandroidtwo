package br.com.frameworktestng.utils.properties;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class projectProperties {

	private String fileProperties;
	private FileInputStream file;
	private Properties properties;
	
	public Properties initializeProperties() throws IOException {
		setFileProperties(".\\src\\test\\resources\\parametros\\dados.properties");
		setFile(new FileInputStream(getFileProperties()));
		setProperties(new Properties());
		getProperties().load(getFile());
		return getProperties();
	}

	public String getFileProperties() {
		return fileProperties;
	}

	public void setFileProperties(String fileProperties) {
		this.fileProperties = fileProperties;
	}

	public FileInputStream getFile() {
		return file;
	}

	public void setFile(FileInputStream file) {
		this.file = file;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}
	
	public Properties getProperties() {
		return properties;
	}
}
