package br.com.frameworktestng.utils.printscreenshort;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;

import br.com.frameworktestng.utils.dateFormats;
import br.com.frameworktestng.utils.drive.driverInitialize;
import br.com.frameworktestng.utils.properties.projectProperties;

public class PrintScreenShort extends driverInitialize{
	
	private String step;
	private File scrFile;
	private Properties prop;
	
	protected static Steps steps;
	protected static dateFormats daFormats;
	protected static projectProperties getProper;

	public PrintScreenShort() {
		try {
			getProper = new projectProperties();
			prop = getProper.initializeProperties();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void getScreenshot(String descriptStep) {
		try {
			
			setStep(Thread.currentThread().getStackTrace()[2].getMethodName());
			setScrFile(getDriverMobile().getScreenshotAs(OutputType.FILE));
			fileUtils();
			steps = new Steps(descriptStep, prop.getProperty("tempImg") + "\\images" + daFormats.datePrintScreen() + "_" + getStep() + ".png");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public FileUtils fileUtils() throws IOException {
		FileUtils fileU = null;
		fileU.copyFile(getScrFile(), new File(prop.getProperty("tempImg") + "\\images" + daFormats.datePrintScreen() + "_" + getStep() + ".png"));
		return fileU;
	}
	
	public String getStep() {
		return step;
	}

	public void setStep(String step) {
		this.step = step;
	}

	public File getScrFile() {
		return scrFile;
	}

	public void setScrFile(File scrFile) {
		this.scrFile = scrFile;
	}
}
