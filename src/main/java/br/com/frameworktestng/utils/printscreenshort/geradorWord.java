package br.com.frameworktestng.utils.printscreenshort;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Properties;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.BreakType;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFPicture;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STHeightRule;

import br.com.frameworktestng.utils.dateFormats;
import br.com.frameworktestng.utils.properties.projectProperties;

public class geradorWord {
	
	private static XWPFDocument document;
	private static XWPFParagraph tmpParagraphHeader;
	private static XWPFParagraph tmpParagraphTable;
	private static XWPFParagraph tmpParagraphTableError;
	private static XWPFParagraph tmpParagraphImg;
	private static XWPFRun tmpRunHeader;
	private static XWPFRun tmpRuntable;
	private static XWPFRun tmpRuntableError;
	private static XWPFRun tmpRunimg;
	private static XWPFTable tableHeader;
	private static XWPFTable tableError;
	private static XWPFTableRow tableRow;
	private static FileOutputStream fos;
	private static FileInputStream isDois;
	private static XWPFPicture picture;
	private static XmlCursor cursor;
	private static Properties prop;
	private static int evidencia;
	
	protected static Steps steps;
	protected static dateFormats dateFormats;
	protected static projectProperties getProper;
	
	public geradorWord() {
		getProper = new projectProperties();
		try {
			prop = getProper.initializeProperties();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void createDoc() {
		document = new XWPFDocument();
		tmpParagraphHeader = document.createParagraph();
		tmpParagraphTable = document.createParagraph();
		tmpParagraphImg = document.createParagraph();
		tmpParagraphTableError = document.createParagraph();
		tmpRunHeader = tmpParagraphHeader.createRun();
		tmpRuntable = tmpParagraphTable.createRun();
		tmpRunimg = tmpParagraphImg.createRun();
		tmpRuntableError = tmpParagraphTableError.createRun();
	}
	
	public static void saveDoc(String nomeDoc) throws FileNotFoundException, IOException {
		
		fos = new FileOutputStream(new File(".\\src\\test\\resources\\evidencias\\" + nomeDoc + ".docx"));
		document.write(fos);
		fos.close();
		
	}
	
	public static void addHeader(String project) {
		tmpRunHeader.setText(project);
		tmpRunHeader.setFontFamily("Times New Roman");
		tmpRunHeader.setBold(true);
		tmpRunHeader.setFontSize(18);
	}
	
	public static void addTable(String nameProject, String nomeCenario) {
		
		tmpRuntable.addBreak();
		cursor = tmpParagraphTable.getCTP().newCursor();//this is the key!
		setTableHeader(document.insertNewTbl(cursor));
		getTableHeader().setWidth(8000);
		addTableRow("Nome do Projeto: ", nameProject, getTableHeader());
		addTableRow("Data de execução: ", dateFormats.atualDataHoraString(), getTableHeader());
		addTableRow("Cenário de Teste: ", nomeCenario, getTableHeader());
		
	}
	
	public static void addTableErro(String defect, String description, String message) {
		
		tmpRuntableError.addBreak();
		cursor = tmpParagraphTableError.getCTP().newCursor();//this is the key!
		setTableError(document.insertNewTbl(cursor));
		getTableError().setWidth(8000);
		addTableRow("Defeito: ", defect, getTableError());
		addTableRow("Descrição: ", description, getTableError());
		addTableRow("Menssagem: ", message, getTableError());
		
	}
	
	public static void addTableRow(String name, String description, XWPFTable table) {
		
		if(table.getRows().get(0).getHeight() == 0) {
			tableRow = table.getRow(0);
			tableRow.getCell(0).setText(name);
			tableRow.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(2000));
			tableRow.addNewTableCell().setText(description);
			tableRow.setHeight((int)(1440*1/5)); //set height 1/10 inch.
			tableRow.getCtRow().getTrPr().getTrHeightArray(0).setHRule(STHeightRule.EXACT); //set w:hRule="exact
		}else {
			tableRow = table.createRow();
			tableRow.getCell(0).setText(name);
			tableRow.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(2000));
			tableRow.getCell(1).setText(description);
			tableRow.setHeight((int)(1440*1/5)); //set height 1/10 inch.
			tableRow.getCtRow().getTrPr().getTrHeightArray(0).setHRule(STHeightRule.EXACT); //set w:hRule="exact
		}
		
	}

	public static void createParPicture(String description, String imgFile) throws InvalidFormatException, IOException {
		if(evidencia == 1) {
			insertPicture(description, imgFile);
			evidencia = evidencia + 1;
		}else if(evidencia == 2){
			tmpRunimg.addBreak(BreakType.PAGE);
			insertPicture(description, imgFile);
			evidencia = 0;
		}else {
			insertPicture(description, imgFile);
			evidencia = evidencia + 1;
		}
	}
	
	public static void insertPicture(String description, String imgFile) throws InvalidFormatException, IOException {
		
			isDois = new FileInputStream(imgFile);
			tmpRunimg.addBreak();
			tmpRunimg.setText(description + ": ");
			tmpRunimg.addBreak();
			picture = tmpRunimg.addPicture(isDois, XWPFDocument.PICTURE_TYPE_JPEG, imgFile, Units.toEMU(110), Units.toEMU(190));
			picture.getCTPicture().getSpPr().addNewLn().isSetSolidFill();
			picture.getCTPicture().getSpPr().getLn().addNewSolidFill().addNewSrgbClr().setVal(new byte[]{0,0,0});
			tmpRunimg.addBreak();			
			isDois.close();
			
	}
	
	//Aqui eu preciso arrumar
	public static void gerador(String nomeCenario, String Step, String erro) {
		try {
			
			createDoc();
			addHeader("Evidências - Automação de Testes");
			addTable(prop.getProperty("projectName"), nomeCenario);
			
//			createParPicture(Step, ".\\src\\test\\resources\\evidencias\\images.jpg");
			
			for (int i = 0; i < steps.descriptAr.size(); i++) {
				createParPicture(steps.descriptAr.get(i), steps.imageAr.get(i));
			}
			
			if(erro != null) {
				addTableErro("Erro", "Elemento diferente do esperado", erro);
			}
			saveDoc(nomeCenario);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static XWPFTable getTableHeader() {
		return tableHeader;
	}

	public static void setTableHeader(XWPFTable tableHeader) {
		geradorWord.tableHeader = tableHeader;
	}

	public static XWPFTable getTableError() {
		return tableError;
	}

	public static void setTableError(XWPFTable tableError) {
		geradorWord.tableError = tableError;
	}
	
	public static void main(String[] args){
		geradorWord ger = new geradorWord();
		ger.gerador("teste", "steps", "erro");
	}

}
