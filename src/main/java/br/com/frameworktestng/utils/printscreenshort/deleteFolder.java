package br.com.frameworktestng.utils.printscreenshort;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;

import br.com.frameworktestng.utils.properties.projectProperties;

public class deleteFolder {

	private Properties prop;
	private File currentFile;
	
	protected static projectProperties getProper;
	
	public deleteFolder() {
		try {
			getProper = new projectProperties();
			prop = getProper.initializeProperties();
			setCurrentFile(new File(prop.getProperty("tempImg")));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void delete() {
		try {
			FileUtils.deleteDirectory(getCurrentFile());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public File getCurrentFile() {
		return currentFile;
	}

	public void setCurrentFile(File currentFile) {
		this.currentFile = currentFile;
	}
    
	public static void main(String[] args){
		deleteFolder del = new deleteFolder();
		del.delete();
	}
}
