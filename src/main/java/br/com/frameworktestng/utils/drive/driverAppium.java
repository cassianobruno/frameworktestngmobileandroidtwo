package br.com.frameworktestng.utils.drive;

public class driverAppium {

	private static String deviceName;
	private static String platformName;
	private static String noReset;
	private static String platformVersion;
	private static String appPackage;
	private static String appActivity;
	private static String hostAppium;
	private static String timeOut;
	
	public static String getDeviceName() {
		return deviceName;
	}
	public static void setDeviceName(String deviceName) {
		driverAppium.deviceName = deviceName;
	}
	public static String getPlatformName() {
		return platformName;
	}
	public static void setPlatformName(String platformName) {
		driverAppium.platformName = platformName;
	}
	public static String getNoReset() {
		return noReset;
	}
	public static void setNoReset(String noReset) {
		driverAppium.noReset = noReset;
	}
	public static String getPlatformVersion() {
		return platformVersion;
	}
	public static void setPlatformVersion(String platformVersion) {
		driverAppium.platformVersion = platformVersion;
	}
	public static String getAppPackage() {
		return appPackage;
	}
	public static void setAppPackage(String appPackage) {
		driverAppium.appPackage = appPackage;
	}
	public static String getAppActivity() {
		return appActivity;
	}
	public static void setAppActivity(String appActivity) {
		driverAppium.appActivity = appActivity;
	}
	public static String getHostAppium() {
		return hostAppium;
	}
	public static void setHostAppium(String hostAppium) {
		driverAppium.hostAppium = hostAppium;
	}
	public static String getTimeOut() {
		return timeOut;
	}
	public static void setTimeOut(String timeOut) {
		driverAppium.timeOut = timeOut;
	}
	
	
}
