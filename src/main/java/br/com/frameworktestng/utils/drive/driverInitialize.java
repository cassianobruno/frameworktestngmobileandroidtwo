package br.com.frameworktestng.utils.drive;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;

import br.com.frameworktestng.utils.properties.projectProperties;
import io.appium.java_client.android.AndroidDriver;

public class driverInitialize {

	private static Properties pro;
	private static AndroidDriver driverMobile;
	private static DesiredCapabilities capabilities;
	
	protected projectProperties getPro;
	protected driverAppium driveApp;
	
	public void initializeProperties() throws IOException {
		getPro = new projectProperties();
		driveApp = new driverAppium();
		
		pro = getPro.initializeProperties();
		
		driveApp.setDeviceName(pro.getProperty("deviceName"));
		driveApp.setPlatformName(pro.getProperty("platformName"));
		driveApp.setNoReset(pro.getProperty("noReset"));
		driveApp.setPlatformVersion(pro.getProperty("platformVersion"));
		driveApp.setAppPackage(pro.getProperty("appPackage"));
		driveApp.setAppActivity(pro.getProperty("appActivity"));
		driveApp.setHostAppium(pro.getProperty("hostAppium"));
		driveApp.setTimeOut(pro.getProperty("timeout"));
	}

	public static AndroidDriver getDriverMobile() {
		return driverMobile;
	}

	public static void setDriverMobile(AndroidDriver driverMobile) {
		driverInitialize.driverMobile = driverMobile;
	}

	public static DesiredCapabilities getCapabilities() {
		return capabilities;
	}

	public static void setCapabilities(DesiredCapabilities capabilities) {
		driverInitialize.capabilities = capabilities;
	}
	
	public void initializeDriver() throws MalformedURLException {
		setCapabilities(new DesiredCapabilities());
		
		getCapabilities().setCapability("deviceName", driveApp.getDeviceName());
		getCapabilities().setCapability("platformName", driveApp.getPlatformName());
		getCapabilities().setCapability("noReset", driveApp.getNoReset());
		getCapabilities().setCapability("platformVersion", driveApp.getPlatformVersion());
		getCapabilities().setCapability("appPackage", driveApp.getAppPackage());
		getCapabilities().setCapability("appActivity", driveApp.getAppActivity());
		
		setDriverMobile(new AndroidDriver(new URL(driveApp.getHostAppium()), getCapabilities()));
	}
	
	public void timeOutDriver(int timeOut) {
		getDriverMobile().manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
	}
	
	public void endsDriver() {
		getDriverMobile().quit();
	}
	
}
