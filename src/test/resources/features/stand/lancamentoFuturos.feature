#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@lancamentoFuturos
Feature: Carroussel com lançamentos futuros
  Sendo cliente do banco Itaú
	Posso entrar na home do aplicativo
	Para consultar meus lançamentos futuros

  @lancamentoFuturos
  Scenario Outline: Validar cliente com lançamento futuro, informações aberta
   		Given que estou no app do banco Itau
		When realizo o login com seguimento <seguimento>
		When navego na home page 
		When valido que o cliente possua lançamento futuro
		Then Valido que as informações estão aberta 
		
	Examples:
		|seguimento|
		|"Varejo"|
		|"Uniclass"|
		|"Person"|
		|"Private"|
		
  @lancamentoFuturos
  Scenario Outline: Validar cliente com lançamento futuro, informações Ocultas
   		Given que estou no app do banco Itau
		When realizo o login com seguimento <seguimento>
		When navego na home page 
		When valido que o cliente possua lançamento futuro
		Then Valido que as informações estão ocultas 
		
	Examples:
		|seguimento|
		|"Varejo"|
		|"Uniclass"|
		|"Person"|
		|"Private"|
		
  @lancamentoFuturos
  Scenario Outline: Validar botão de direcionamento com cliente que tenha lançamento futuros
   		Given que estou no app do banco Itau
		When realizo o login com seguimento <seguimento>
		When navego na home page 
		When valido que o cliente possua lançamento futuro
		Then Valido o botão de direcionamento 
		
	Examples:
		|seguimento|
		|"Varejo"|
		|"Uniclass"|
		|"Person"|
		|"Private"|
		
  @lancamentoFuturos
  Scenario Outline: Validar cliente sem lançamento futuro, informações aberta
   		Given que estou no app do banco Itau
		When realizo o login com seguimento <seguimento>
		When navego na home page 
		When valido que o cliente não possua lançamento futuro
		Then Valido que as informações estão aberta 
		
	Examples:
		|seguimento|
		|"Varejo"|
		|"Uniclass"|
		|"Person"|
		|"Private"|
		
  @lancamentoFuturos
  Scenario Outline: Validar cliente sem lançamento futuro, informações Ocultas
   		Given que estou no app do banco Itau
		When realizo o login com seguimento <seguimento>
		When navego na home page 
		When valido que o cliente não possua lançamento futuro
		Then Valido que as informações estão ocultas 
		
	Examples:
		|seguimento|
		|"Varejo"|
		|"Uniclass"|
		|"Person"|
		|"Private"|
		
  @lancamentoFuturos
  Scenario Outline: Validar botão de direcionamento com cliente que não tenha lançamento futuros
   		Given que estou no app do banco Itau
		When realizo o login com seguimento <seguimento>
		When navego na home page 
		When valido que o cliente não possua lançamento futuro
		Then Valido o botão de direcionamento 
		
	Examples:
		|seguimento|
		|"Varejo"|
		|"Uniclass"|
		|"Person"|
		|"Private"|
		
		
  @lancamentoFuturos
  Scenario Outline: Validar cliente com lançamento futuro utilizando o LIS, informações aberta
   		Given que estou no app do banco Itau
		When realizo o login com seguimento <seguimento>
		When navego na home page 
		When valido que o cliente possua lançamento futuro
		Then Valido que as informações estão aberta 
		Then valido que o cliente esta utilizando o LIS
		
	Examples:
		|seguimento|
		|"Varejo"|
		|"Uniclass"|
		|"Person"|
		|"Private"|
		
  @lancamentoFuturos
  Scenario Outline: Validar cliente com lançamento futuro utilizando o LIS, informações Ocultas
   		Given que estou no app do banco Itau
		When realizo o login com seguimento <seguimento>
		When navego na home page 
		When valido que o cliente possua lançamento futuro
		Then Valido que as informações estão ocultas
		Then valido que o cliente esta utilizando o LIS
		
	Examples:
		|seguimento|
		|"Varejo"|
		|"Uniclass"|
		|"Person"|
		|"Private"|
		
  @lancamentoFuturos
  Scenario Outline: Validar botão de direcionamento com cliente que tenha lançamento futuros utilizando o LIS
   		Given que estou no app do banco Itau
		When realizo o login com seguimento <seguimento>
		When navego na home page 
		When valido que o cliente possua lançamento futuro
		Then Valido o botão de direcionamento
		Then valido que o cliente esta utilizando o LIS
		
	Examples:
		|seguimento|
		|"Varejo"|
		|"Uniclass"|
		|"Person"|
		|"Private"|
		
  @lancamentoFuturos
  Scenario Outline: Validar cliente sem lançamento futuro utilizando o LIS, informações aberta
   		Given que estou no app do banco Itau
		When realizo o login com seguimento <seguimento>
		When navego na home page 
		When valido que o cliente não possua lançamento futuro
		Then Valido que as informações estão aberta
		Then valido que o cliente esta utilizando o LIS
		
	Examples:
		|seguimento|
		|"Varejo"|
		|"Uniclass"|
		|"Person"|
		|"Private"|
		
  @lancamentoFuturos
  Scenario Outline: Validar cliente sem lançamento futuro utilizando o LIS, informações Ocultas
   		Given que estou no app do banco Itau
		When realizo o login com seguimento <seguimento>
		When navego na home page 
		When valido que o cliente não possua lançamento futuro
		Then Valido que as informações estão ocultas
		Then valido que o cliente esta utilizando o LIS
		
	Examples:
		|seguimento|
		|"Varejo"|
		|"Uniclass"|
		|"Person"|
		|"Private"|
		
  @lancamentoFuturos
  Scenario Outline: Validar botão de direcionamento com cliente que não tenha lançamento futuros utilizando o LIS
   		Given que estou no app do banco Itau
		When realizo o login com seguimento <seguimento>
		When navego na home page 
		When valido que o cliente não possua lançamento futuro
		Then Valido o botão de direcionamento
		Then valido que o cliente esta utilizando o LIS
		
	Examples:
		|seguimento|
		|"Varejo"|
		|"Uniclass"|
		|"Person"|
		|"Private"|
		

  @lancamentoFuturos
  Scenario Outline: Validar cliente com lançamento futuro utilizando o AD, informações aberta
   		Given que estou no app do banco Itau
		When realizo o login com seguimento <seguimento>
		When navego na home page 
		When valido que o cliente possua lançamento futuro
		Then Valido que as informações estão aberta 
		Then valido que o cliente esta utilizando o AD
		
	Examples:
		|seguimento|
		|"Varejo"|
		|"Uniclass"|
		|"Person"|
		|"Private"|
		
  @lancamentoFuturos
  Scenario Outline: Validar cliente com lançamento futuro utilizando o AD, informações Ocultas
   		Given que estou no app do banco Itau
		When realizo o login com seguimento <seguimento>
		When navego na home page 
		When valido que o cliente possua lançamento futuro
		Then Valido que as informações estão ocultas
		Then valido que o cliente esta utilizando o AD
		
	Examples:
		|seguimento|
		|"Varejo"|
		|"Uniclass"|
		|"Person"|
		|"Private"|
		
  @lancamentoFuturos
  Scenario Outline: Validar botão de direcionamento com cliente que tenha lançamento futuros utilizando o AD
   		Given que estou no app do banco Itau
		When realizo o login com seguimento <seguimento>
		When navego na home page 
		When valido que o cliente possua lançamento futuro
		Then Valido o botão de direcionamento
		Then valido que o cliente esta utilizando o AD
		
	Examples:
		|seguimento|
		|"Varejo"|
		|"Uniclass"|
		|"Person"|
		|"Private"|
		
  @lancamentoFuturos
  Scenario Outline: Validar cliente sem lançamento futuro utilizando o AD, informações aberta
   		Given que estou no app do banco Itau
		When realizo o login com seguimento <seguimento>
		When navego na home page 
		When valido que o cliente não possua lançamento futuro
		Then Valido que as informações estão aberta
		Then valido que o cliente esta utilizando o AD
		
	Examples:
		|seguimento|
		|"Varejo"|
		|"Uniclass"|
		|"Person"|
		|"Private"|
		
  @lancamentoFuturos
  Scenario Outline: Validar cliente sem lançamento futuro utilizando o AD, informações Ocultas
   		Given que estou no app do banco Itau
		When realizo o login com seguimento <seguimento>
		When navego na home page 
		When valido que o cliente não possua lançamento futuro
		Then Valido que as informações estão ocultas
		Then valido que o cliente esta utilizando o AD
		
	Examples:
		|seguimento|
		|"Varejo"|
		|"Uniclass"|
		|"Person"|
		|"Private"|
		
  @lancamentoFuturos
  Scenario Outline: Validar botão de direcionamento com cliente que não tenha lançamento futuros utilizando o AD
   		Given que estou no app do banco Itau
		When realizo o login com seguimento <seguimento>
		When navego na home page 
		When valido que o cliente não possua lançamento futuro
		Then Valido o botão de direcionamento
		Then valido que o cliente esta utilizando o AD
		
	Examples:
		|seguimento|
		|"Varejo"|
		|"Uniclass"|
		|"Person"|
		|"Private"|
		
		
  @lancamentoFuturos
  Scenario Outline: Validar agendamento do lançamento futuro
   		Given que estou no app do banco Itau
		When realizo o login com seguimento <seguimento>
		When navego na home page 
		When valido que o cliente não possua lançamento futuro
		Then Valido o botão de direcionamento
		Then valido que o cliente tem agendamento para lançamento futuros
		
	Examples:
		|seguimento|
		|"Varejo"|
		|"Uniclass"|
		|"Person"|
		|"Private"|
		

  @lancamentoFuturos
  Scenario Outline: Realizar o cancelamento do agendamento do lançamento futuro
   		Given que estou no app do banco Itau
		When realizo o login com seguimento <seguimento>
		When navego na home page 
		When valido que o cliente não possua lançamento futuro
		Then Valido o botão de direcionamento
		Then valido que o cliente tem agendamento e efetuo o cancelamento do mesmo
		
	Examples:
		|seguimento|
		|"Varejo"|
		|"Uniclass"|
		|"Person"|
		|"Private"|