#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@splash
Feature: Splash Itau
  Sendo cliente do banco Itaú
	Posso entrar no splash de abertura do aplicativo
	Para validar o recado apresentado ao cliente

  @splash
  Scenario: Validar splash do banco Itau
   		Given que estou no app do banco Itau
		When e apresentando a tela de splash
		Then Valido que as informações da tela de splash