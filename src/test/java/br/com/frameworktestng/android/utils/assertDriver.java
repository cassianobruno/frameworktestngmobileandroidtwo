package br.com.frameworktestng.android.utils;

import org.testng.Assert;

import br.com.frameworktestng.android.page.splashPage;
import br.com.frameworktestng.utils.MessageUtil;
import br.com.frameworktestng.utils.listener.Reporter;
import br.com.frameworktestng.utils.printscreenshort.PrintScreenShort;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class assertDriver {

	protected PrintScreenShort printScreenShort;
	protected MessageUtil messageUtil;
	
	private String assertionError;
	
	public assertDriver() {
		messageUtil = new MessageUtil();
		printScreenShort = new PrintScreenShort();
	}
	
	public void validarTextAssert(String text, String textComparative, String descriptionScrrenShort) {
		try {
        	Assert.assertEquals(text, textComparative);
        	
        	printScreenShort.getScreenshot(descriptionScrrenShort);
    		
    		Reporter.addStepLog("My test addStepLog message");
            Reporter.addScenarioLog("This is scenario log");
        }
        catch (AssertionError ae) {
        	assertionError = ae.getMessage();
            messageUtil.setMessage(assertionError);
            messageUtil.printMessage();
            
            Assert.assertTrue(false);
        }
	}
}
