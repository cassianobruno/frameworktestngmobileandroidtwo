package br.com.frameworktestng.android.page;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class splashPage{
	
	private AndroidDriver<AndroidElement> driver;

	public splashPage(AndroidDriver<AndroidElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@AndroidFindBy(xpath = "//android.widget.LinearLayout[@content-desc=\"Seja bem-vindo ;). Estamos sempre atualizando o app Itaú para trazer mais praticidade no seu dia a dia\"]/android.widget.TextView[1]")
	private AndroidElement title;
	
	@AndroidFindBy(xpath = "//android.widget.LinearLayout[@content-desc=\"Seja bem-vindo ;). Estamos sempre atualizando o app Itaú para trazer mais praticidade no seu dia a dia\"]/android.widget.TextView[2]")
	private AndroidElement description;
	
	@AndroidFindBy(xpath = "//android.widget.Button[@content-desc=\"abrir uma conta\"]")
	private AndroidElement buttonOpenAnAccount;
	
	@AndroidFindBy(xpath = "//android.widget.Button[@content-desc=\"acessar minha conta\"]")
	private AndroidElement buttonAccessMyAccount;
	
	public String getTitle() {
		String titleSplash = this.title.getText();
		return titleSplash;
	}
	
	public String getDescription() {
		String titleSplash = this.description.getText();
		return titleSplash;
	}
	
	public String getButtonOpenAnAccount() {
		String titleSplash = this.buttonOpenAnAccount.getText();
		return titleSplash;
	}
	
	public String getButtonAccessMyAccount() {
		String titleSplash = this.buttonAccessMyAccount.getText();
		return titleSplash;
	}
}
