package br.com.frameworktestng.android.steps;

import java.io.IOException;
import java.lang.AssertionError;

import org.testng.Assert;

import br.com.frameworktestng.utils.MessageUtil;
import br.com.frameworktestng.utils.drive.driverInitialize;
import br.com.frameworktestng.utils.listener.Reporter;
import br.com.frameworktestng.utils.printscreenshort.PrintScreenShort;
import br.com.frameworktestng.utils.printscreenshort.deleteFolder;
import br.com.frameworktestng.utils.printscreenshort.geradorWord;
import br.com.frameworktestng.android.page.splashPage;
import br.com.frameworktestng.android.sections.splashSections;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class splashSteps {

	protected driverInitialize driverInit;
	protected geradorWord geradorword;
	protected deleteFolder deletefolder;
	protected PrintScreenShort printScreenShort;
	protected splashSections sections;
	
	private MessageUtil messageUtil;
	private String assertionError = null;
	
	@Before
    public void beforeScenario(Scenario scenario) throws IOException {
		driverInit = new driverInitialize();
        messageUtil = new MessageUtil();
        printScreenShort = new PrintScreenShort();
        deletefolder = new deleteFolder();
        geradorword = new geradorWord();
        
	    driverInit.initializeProperties();
	    driverInit.initializeDriver();
	    
		if (scenario.getName().equals("Validação do Splash")) {
            Reporter.assignAuthor("Bruno Cassiano");
        }
    }
	
	@After("@splash")
	public void leave_window_open(Scenario scenario) throws IOException {
		if (scenario.isFailed()) {
			printScreenShort.getScreenshot("Erro durante a execução");
			this.geradorword.gerador(scenario.getId().split(";")[0].replace("-", " "), scenario.getName(), messageUtil.getMessage());
			this.deletefolder.delete(); 
		} else {
			this.geradorword.gerador(scenario.getId().split(";")[0].replace("-", " "), scenario.getName(), null);
			this.deletefolder.delete();
		}
	}
	
	@Given("^que estou no app do banco Itau$")
	public void que_estou_no_app_do_banco_Itau() throws Throwable {
		sections = new splashSections(driverInit.getDriverMobile());
		sections.splashValidadeTitle("Seja bem-vindo ;)");
	}

	@When("^e apresentando a tela de splash$")
	public void e_apresentando_a_tela_de_splash() throws Throwable {
		sections = new splashSections(driverInit.getDriverMobile());
		sections.splashValidadeDescription("Estamos sempre atualizando o app Itaú para trazer mais praticidade no seu dia a dia.");
	}

	@Then("^Valido que as informações da tela de splash$")
	public void valido_que_as_informações_da_tela_de_splash() {
		sections = new splashSections(driverInit.getDriverMobile());
		sections.splashValidadeButtonOpenAnAccount("abrir uma conta");
		sections.splashValidadeButtonAcessMyAccount("acessar minha conta1");
	}
}
