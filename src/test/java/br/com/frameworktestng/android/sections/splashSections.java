package br.com.frameworktestng.android.sections;

import org.testng.Assert;

import br.com.frameworktestng.android.page.splashPage;
import br.com.frameworktestng.android.utils.assertDriver;
import br.com.frameworktestng.utils.MessageUtil;
import br.com.frameworktestng.utils.listener.Reporter;
import br.com.frameworktestng.utils.printscreenshort.PrintScreenShort;
import br.com.frameworktestng.utils.properties.projectProperties;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class splashSections {

	protected splashPage spPage;
	protected assertDriver aDriver;
	
	public splashSections(AndroidDriver<AndroidElement> driver) {
		spPage = new splashPage(driver);
		aDriver = new assertDriver();
	}
	
	public void splashValidadeTitle(String title) {
		aDriver.validarTextAssert(spPage.getTitle(), title, "Validar o titulo na tela de splash");
	}
	
	public void splashValidadeDescription(String description) {
		aDriver.validarTextAssert(spPage.getDescription(), description, "Validar a descrição na tela de splash");
	}
	
	public void splashValidadeButtonOpenAnAccount(String button) {
		aDriver.validarTextAssert(spPage.getButtonOpenAnAccount(), button, "Validar o botão de abertura de conta na tela de splash");
	}

	public void splashValidadeButtonAcessMyAccount(String button) {
		aDriver.validarTextAssert(spPage.getButtonAccessMyAccount(), button, "Validar o botão de acesso a conta na tela de splash");
	}
}
